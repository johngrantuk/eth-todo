# DApp Challenge

So you want to work with Stability Labs? Well first things first, let's see how
you solve problems.

If you're a seasoned coder, you should have no problems with the task, but don't
worry if you're new to the game either, we just want to see is how you solve
problems while interacting with Ethereum.

We really like to see clean interfaces and well written, modular code -
particularly when dealing with the quite complicated web3 connection.

## Instructions

Create a todo list application with the following components:

- Smart Contract - this should be the storage for all of the todo items that users are going to create
- Frontend - this should allow you to interact with the todo list from a web interface, handling the result and state of any transactions

## Suggestions

- Use Truffle for tweaking and deploying the smart contract
- Use Ganache-cli as the test blockchain
- Use React and Context API to act as the client
- Use Web3js or Ethers library (or `web3-react`) to connect to Ethereum and interact with the deployed smart contracts
- Put a dash of colour on top with an elegant CSS library

## Submissions

Submit your work as a pull request to this repository and shoot an email with the link to alex@stabilitylabs.co, we'll get back to you as soon as we can.
